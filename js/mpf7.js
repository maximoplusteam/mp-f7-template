var myApp = new Framework7();

var $$ = Dom7;

$$("#login_button").on("click", function(e){
  maximoplus.core.max_login(document.getElementById("login_username").value,
			    document.getElementById("login_password").value
			   ,function(ok){
		              myApp.closeModal();
                              document.location.reload();
			    },
			    function(err){
			      maximoplus.core.handleErrorMessage("Invalid Username or Password");
			    }
			   )
});


function goToPage(view,pageId){
  view.router.load({pageName:pageId})
}

function goBack(view){
  view.router.back();
}

maximoplus.core.globalFunctions.handleErrorMessage = function(text,optEventTarget){myApp.alert(text,"Error");}

maximoplus.core.globalFunctions.global_login_function = function(){
  myApp.loginScreen();
}



function F7List(container,columns,norows, lineTemp,view,detailsPage){
  maximoplus.controls.AbstractGrid.call(this,container,columns,norows);
  this.lineTemo=lineTemp;
  this.detailsPage=detailsPage;
  this.view = view;
  //template7 for the line
  var templ=$$("#"+lineTemp).html();
  this.lineTemplate=Template7.compile(templ);
  
}

goog.inherits(F7List, maximoplus.controls.AbstractGrid);

F7List.prototype.gridToolbar=function(){}

F7List.prototype.listenSelectAction = function(selectF){
  //TO BE DONE
  console.log("TO be done, f7 list listening");
}

F7List.prototype.getRowControl = function(mxrow,disprow){
  return new F7Line(this.container, this.columns, mxrow,disprow);
}

F7List.prototype.getToolbar = function(){}
F7List.prototype.getQbeRow = function(){}
F7List.prototype.getLabelRow=function(){}
F7List.prototype.qbeRow=function(){}
F7List.prototype.headerRow=function(){}

F7List.prototype.mainGridFrame = function(){
  var el=document.createElement("div");
  el.innerHTML="<div class='list-block'><ul></ul></div>";
  var ret= el.firstChild;
  this.getElement().appendChild(ret);
  return ret;
}

F7List.prototype.getRowContainer = function(){
  return this.getElement().children[0].children[0];
}

F7List.prototype.initData = function(){
  var prom=maximoplus.controls.AbstractGrid.prototype.initData.call(this);
  var that=this;
  return prom
         .then(function(_){return that.mbosetcnt;})
	 .then(function(v){that.addInfiniteScroll();});
}

F7List.prototype.addInfiniteScroll = function(){
  var rowsToFetch = this.tableRowsCount();
  var rowsFetched = this.getNumrows();
  if (rowsFetched>=rowsToFetch){
    return;//came to the end of fetch, don't add the infinite scroll
  }
  this.scrollerAttached=true;
  if (this.scroller){
    myApp.attachInfiniteScroll(this.scroller);
    this.scroller.querySelector(".infinite-scroll-preloader").style.removeProperty("display");
    return;
  }
  var scroller = closest(this.getElement(),".infinite-scroll");
  scroller.querySelector(".infinite-scroll-preloader").style.removeProperty("display");
  var that=this;
  if (scroller){
    this.scroller=scroller;
    $$(scroller).on("infinite", function(){
      if (!that.fetching && !that.scrollerDetached){
        that.fetching=true;
        that.fetchMore(5).then(function(_){that.fetching=false;});
      }
    });
    
  }
}

F7List.prototype.removeInfiniteScroll = function(){
  this.scroller.querySelector(".infinite-scroll-preloader").style.display="none";
  myApp.detachInfiniteScroll($$(this.scroller));
  this.scrollerAttached=false;
  this.scrollerDetached=true;
}

F7List.prototype.prepareCall = function(command, eventTarget){
  if (command ==="fetch"){
    if (eventTarget.scrollerAttached){
      //scroller already has the preloader displayed at the end of the list
      return;
    }
    var scroller = closest(eventTarget.getElement(),".infinite-scroll");
    if (scroller) scroller.querySelector(".infinite-scroll-preloader").style.removeProperty("display");
  }
}

F7List.prototype.finishCall = function(command,eventTarget){
  if (command === "fetch"){
    if (eventTarget.scrollerAttached){
      var rowsToFetch = eventTarget.tableRowsCount();
      var rowsFetched = eventTarget.getNumrows();
      if (rowsFetched>=rowsToFetch){
        eventTarget.removeInfiniteScroll();//came to the end of fetch, don't add the infinite scroll
      }
    } else{
      var scroller = closest(eventTarget.getElement(),".infinite-scroll");
      if (scroller) scroller.querySelector(".infinite-scroll-preloader").style.display="none";
    }
  }
}

function F7Line (container,columns,mxrow,disprow){
  maximoplus.controls.AbstractLine.call(this,container,columns,mxrow,disprow);
}

goog.inherits(F7Line,maximoplus.controls.AbstractLine);

F7Line.prototype.setEnabled = function(enable){}

F7Line.prototype.onSetReadonly = function(){}

F7Line.prototype.setRowValues  = function(colvals){
  var template=this.getParent().lineTemplate;
  this.getElement().firstElementChild.firstElementChild.innerHTML=template(colvals);
}

F7Line.prototype.setRowValue = function(column,value){
  var ldata  = this.getContainer().getLocalData();
  var template = this.getParent().lineTemplate;
  if (ldata){
    this.getElement().firstElementChild.firstElementChild.innerHTML=template(ldata);
  }
}

F7Line.prototype.setFieldFlag = function(field,flag){}

F7Line.prototype.setFieldFlags = function(colflags){}

F7Line.prototype.highlightSelectedRow = function(){
}


F7Line.prototype.unhighlightSelectedRow = function(){
}

F7Line.prototype.listenRow = function(selectF){
  var that=this;
  $$(this.getElement()).on("click", function(ev){
    selectF.call(that);
  });
}

F7Line.prototype.createComponentDom = function(){
  var el=document.createElement("div");
  el.innerHTML="<li class='item-content'><div class='item-inner'><div class='item-title'></div></div></li>";
  return el.firstElementChild;
}

F7Line.prototype.enteredDocument = function(){
  maximoplus.controls.AbstractLine.prototype.enteredDocument.call(this);
  var detailsPage  = this.getParent().detailsPage;
  var view = this.getParent().view;

  if (detailsPage){
    $$(this.getElement()).on("click",function(){
      goToPage(view,detailsPage);
    });
  }
  
}

function closest(el, selector) {
  while (el!==null) {
    parent = el.parentElement;
    if (parent!==null && parent.matches(selector)) {
      return parent;
    }
    else{
      el=parent;
    }
  }

  return null;
}

F7TextField = function(metadata,id){
  maximoplus.controls.TextField.call(this,metadata,id);
}

goog.inherits(F7TextField, maximoplus.controls.TextField);

F7TextField.prototype.createComponentDom = function(){
  var dateField=this.metadata.maxType && (this.metadata.maxType == "DATE" || this.metadata.maxType =="DATETIME");
  var label=this.metadata.title;
  var lookupHTML = this.metadata.hasLookup?'<div class="item-media"><i class="lookup" ></i></div>':'<div class="tem-media"></div>';
  var dom='<li> <div class="item-content">' + lookupHTML +' <div class="item-inner"> <div class="item-title label">'+label+'</div> <div class="item-input"> <input type="text"> </div> </div> </div> </li>';
  var _el=document.createElement("div");
  _el.innerHTML=dom;
  var el=_el.firstElementChild;
  this.labelDom=el.querySelector(".label");
  this.activeDom=el.querySelector("input");
  if (dateField){
    el.getElementsByTagName("input")[0].readOnly=true;
    //date fields should be populated just via the calendar
    this.calendar=myApp.calendar({
      input:this.activeDom,
      dateFormat:'dd-M-yyyy',
      closeByOutsideClick:false,
      scrollToInput:false,
      onDayClick:function (p, dayContainer, year, month, day){
        p.close();
      }
    });
    
  }
  return el;
}

F7TextField.prototype.enteredDocument = function(){
  maximoplus.controls.TextField.prototype.enteredDocument.call(this);
  var that=this;
  var el = this.getElement();
  if (this.metadata.hasLookup){
    var iconButton = $$(el).find(".item-media");
    $$(iconButton).on("click", function(ev){
      that.showLookup();
    });
  }

}

F7TextField.prototype.getListDialog = function(listContainer,columns){
  var dialog = new F7ListDialog(this.getContainer(),listContainer,this,columns);
  dialog.renderDeferred();
  return dialog;
}


F7TextField.prototype.getActiveDom = function(){
  return this.activeDom;
}

F7TextField.prototype.setReadonly = function(readOnly){
  var dateField=this.metadata.maxType && (this.metadata.maxType == "DATE" || this.metadata.maxType =="DATETIME");
  if (dateField){
    return;
  }
  maximoplus.controls.TextField.prototype.setReadonly.call(this, readOnly);
  if (readOnly){
    this.getElement().classList.add("disabled");
  }else{
    this.getElement().classList.remove("disabled");
  }
}

F7Section = function(container,columns){
  maximoplus.controls.Section.call(this,container,columns);
}

goog.inherits(F7Section,maximoplus.controls.Section);

F7Section.prototype.createField = function(colMetadata){
  //for the time being, just the text field
  if (colMetadata["radiobutton"]){
    var pickerkeycol=colMetadata["pickerkeycol"];
    var pickercol = colMetadata["pickercol"];
    return new F7RadioButton(colMetadata,pickerkeycol, pickercol,10);
  }
  return new F7TextField(colMetadata);
}

F7Section.prototype.createComponentDom = function(){
  var el=document.createElement("div");
  el.innerHTML="<div class='list-block'> <ul></ul></div>";
  //this is required by F7
  return el.firstElementChild;
}

F7Section.prototype.getRootElementForChildren = function(){
  //this is a method directly from google closure library (one of the few left)
  return this.getElement().firstElementChild; // returns the ul 
}


F7RadioButton=function(metadata,pickerKeyCol,pickerCol,noRows){
  maximoplus.controls.RadioButton.call(this, metadata,pickerKeyCol, pickerCol, noRows);
}



goog.inherits(F7RadioButton, maximoplus.controls.RadioButton);

F7RadioButton.prototype.createComponentDom = function(){
  return document.createElement("div");
  //nothing is required from F7 for the top level
}

F7RadioButton.prototype.getPickerList= function(column, listContainer, pickerKeyCol, pickerCol, noRows){
  return new F7RadioButtonList(column, listContainer, pickerKeyCol, pickerCol, noRows);
}

F7RadioButton.prototype.displayPickerHeader =  function(metadata){
  var el = this.getElement();
  var title = metadata["title"];
  var nel = document.createElement("div");
  nel.innerHTML="<div class='content-block-title'>"+title+"</div>";
  el.insertBefore(nel.firstElementChild, el.firstChild);
}

F7RadioButtonList =function(column, container, pickerKeyCol, pickerCol, noRows){
  maximoplus.controls.RadioButtonList.call(this, column, container, pickerKeyCol, pickerCol, noRows);
}

goog.inherits(F7RadioButtonList, maximoplus.controls.RadioButtonList);

F7RadioButtonList.prototype.getRowControl = function(row, disprow){
  return new F7RadioButtonItem(this.container, this.pickerkeycol, this.pickercol, row, disprow);
}

F7RadioButtonList.prototype.listenListAction = function(callback){
  this.getElement().addEventListener("change", callback);
}

F7RadioButtonList.prototype.getRowControlFromEvent = function(ev){
  return ev.target.parentElement.parentElement.controlRef;//control ref is set on the root of the item(li node)
}

F7RadioButtonList.prototype.createMainListDom = function(){
  var el = document.createElement("div");
  el.innerHTML="<div class='list-block'><ul></ul></div>";
  return el.firstElementChild;
}

F7RadioButtonList.prototype.renderRow = function(row){
  row.renderDeferred(this.getElement().firstElementChild);
}

F7RadioButtonItem = function(container, pickerkeycol, pickercol, mxrow, disprow){
  maximoplus.controls.RadioButtonItem.call(this, container, pickerkeycol, pickercol, mxrow, disprow);
}

goog.inherits(F7RadioButtonItem, maximoplus.controls.RadioButtonItem);

F7RadioButtonItem.prototype.createComponentDom = function(){
  var el=document.createElement("li");
  el.innerHTML="<label class='label-radio item-content'><input type='radio'><div class='item-inner'><div class='item-title'></div></div></label>";
  //  el.setAttribute("name", this.getId());
  el.controlRef=this;
  return el;
}

F7RadioButtonItem.prototype.getActiveDom = function(){
  return this.getElement();
}

F7RadioButtonItem.prototype.pick =function(){
  var el=this.getElement();
  el.getElementsByTagName("input")[0].checked=true;
}

F7RadioButtonItem.prototype.unpick=function(){
  var el=this.getElement();
  el.getElementsByTagName("input")[0].checked=false;
  //DEBUG!!
  console.log("UNPICKING: " +el.getElementsByClassName("item-title")[0].innerHTML);
}

F7RadioButtonItem.prototype.setRowPickerValue = function(value){
  var el=this.getElement();
  var _input=el.getElementsByTagName("input")[0];
  _input.setAttribute("value",value);
  var _title = el.getElementsByClassName("item-title")[0];
  _title.innerHTML=value;
}

F7WorkflowControl = function(appContainer,processName){
  maximoplus.controls.WorkflowControl.call(this, appContainer, processName);
}

goog.inherits(F7WorkflowControl, maximoplus.controls.WorkflowControl);

F7WorkflowControl.prototype.getWfDialog = function(){
  //this should return the div of the dialog. Since only one wf dialog can be opened, there is no need to create it automatically, I will have the separate div for this
  var el=document.getElementById("workflowcont");
  el.innerHTML="<div class='wf-title'></div><div class='wf-body'></div><div class='wf-buttons'></div>";
  //goToPage(mainView,"workflow");
  return el;
}

//F7WorkflowControl.prototype.wfFinished = function(){
//  //just close the dialog, return to the main course of the application
//}

F7WorkflowControl.prototype.setWfDialogTitle=function(dialog,title){
  dialog.getElementsByClassName("wf-title")[0].innerHTML=title;
}

F7WorkflowControl.prototype.addWfDialogAction = function(dialog, f , label, key){
  //Add the necessary buttons here
  var buttonCont = dialog.getElementsByClassName("wf-buttons")[0];
  var el=document.createElement("div");
  var buttonclass="button";
  if ("ok"=== key){
    buttonclass+=" active";
  }
  el.innerHTML='<p><a href="#" class="'+buttonclass+'">'+label+'</a></p>';
  var button=el.firstChild;
  var g = function(ev){
    $$(".wf-buttons .button").addClass("disabled");
    $$(".wf-buttons p").off("click",g);
    f(ev);
  }
  $$(button).on("click", g);
  buttonCont.appendChild(button);
}

F7WorkflowControl.prototype.addWfMemoGrid=function(dialog, memoContainer){
  //No memo grids in this demo, check the other demos for that (ex -Polymer)
}

F7WorkflowControl.prototype.addWfSectionToDialog = function(dialog,section){
  //here render the section inside the dialog
  var wfDialBody = dialog.getElementsByClassName("wf-body")[0];
  wfDialBody.innerHTML = "";
  section.renderDeferred(wfDialBody);
}

F7WorkflowControl.prototype.cancelWf=function(dialog){
  
  goBack(dialog.closest(".view").f7View);
}

F7WorkflowControl.prototype.getWfSection = function(container,objectName,inputFields){
  var s = new F7Section(container,inputFields);
  s.createField=function(metadata){
    var attrName=metadata.attributeName;
    if (attrName == "ACTIONID"){
      return new F7RadioButton(metadata,"actionid","instruction",10);
    }
    return new F7TextField(metadata);
  }
  return s;
}

F7WorkflowControl.prototype.setWarnings = function(warnings, body, title){
  //display the warnings from the worklfow
  if (warnings && warnings.length!=0){
    //here it will be displayed like in Maximo, it will disappear after couple of seconds
    var warnstr="";
    for (var j=0;j<warnings.length;j++){
      warnstr+=warnings[j]+" ";
    }
    var n=myApp.addNotification({title:warnstr});
    var that=this;
    setTimeout(function(_){
      myApp.closeNotification(n);
      goBack(that.getWfDialog().closest(".view").f7View);
                          }, 5000);
  }
  if (body||title){
    maximoplus.core.globalFunctions.handleErrorMessage(body+title);
  }
}

maximoplus.core.globalFunctions.globalDisplayWaitCursor=function(){
  myApp.showIndicator();
}

maximoplus.core.globalFunctions.globalRemoveWaitCursor=function(){
  myApp.hideIndicator();
}

F7ListDialog = function(container, listContainer, field, dialogcols){
  maximoplus.controls.AbstractListDialog.call(this, container, listContainer, field, dialogcols);
  
  this.dialogTemplate=$$("#"+field.metadata.dialogTemplate).html();
  this.lineTemplate=field.metadata.lineTemplate;//already template parsing is done for the line
}

goog.inherits(F7ListDialog, maximoplus.controls.AbstractListDialog);

F7ListDialog.prototype.drawDialog = function(){
  this.currentDialog = myApp.popup(this.dialogTemplate);
  return this.currentDialog;
}

F7ListDialog.prototype.closeListDialog = function(){
  myApp.closeModal(this.currentDialog);
}

F7ListDialog.prototype.getSelectableGrid = function(listContainer, dialogCols, selectableF){
  var sGrid = new F7List(listContainer, dialogCols, 30, this.lineTemplate,myApp.getCurrentView());
  sGrid.selectableF = selectableF;
  this.list = sGrid;
  return sGrid;
}

F7ListDialog.prototype.drawGridInDialog = function(listContainer, listGrid){
  
  var el = this.getElement();
  var contentContainer = $$(el).find(".dialog-content")[0];
  if (contentContainer){
    listGrid.renderDeferred(contentContainer);
  }
  var closeButton = $$(el).find(".dialog-close-button");
  if (closeButton){
    var that=this;
    $$(closeButton).once("click",function(ev){
      that.closeAction();
    });
  }
}

F7QbeField = function(metadata){
  maximoplus.controls.QbeField.call(this,metadata);
}

goog.inherits(F7QbeField,maximoplus.controls.QbeField);

F7QbeField.prototype.enteredDocument = function(){
  maximoplus.controls.QbeField.prototype.enteredDocument.call(this);
  var that=this;
  var el = this.getElement();
  if (this.metadata.hasLookup){
    var iconButton = $$(el).find(".item-media");
    $$(iconButton).on("click", function(ev){
      that.showLookup();
    });
  }
  
}

F7QbeField.prototype.getListDialog = function(listContainer,columns){
  var dialog=new F7QbeListDialog(this.getContainer(), listContainer, this, columns);
  dialog.renderDeferred();
  return dialog;
}



F7QbeField.prototype.createComponentDom = function(){
  return F7TextField.prototype.createComponentDom.call(this);
}

F7QbeField.prototype.getActiveDom = function(){
  return F7TextField.prototype.getActiveDom.call(this);
}

F7QbeSection = function(container, columns){
  maximoplus.controls.AbstractQbeSection.call(this, container, columns);
}


goog.inherits(F7QbeSection, maximoplus.controls.QbeSection);

F7QbeSection.prototype.createField = function(colMetadata){
  return new F7QbeField(colMetadata);
}

F7QbeSection.prototype.createComponentDom = function(){
  return F7Section.prototype.createComponentDom.call(this);
}

F7QbeSection.prototype.getRootElementForChildren = function(){
  return F7Section.prototype.getRootElementForChildren.call(this);
}

F7QbeListDialog = function(container, listContainer, field, dialogcols){
  maximoplus.controls.AbstractQbeListDialog.call(this, container, listContainer, field, dialogcols);
  this.dialogTemplate=$$("#"+field.metadata.dialogTemplate).html();
  this.lineTemplate=field.metadata.lineTemplate;//already template parsing is done for the line
}

goog.inherits(F7QbeListDialog, maximoplus.controls.AbstractQbeListDialog);

F7QbeListDialog.prototype.drawDialog = function(){
  return F7ListDialog.prototype.drawDialog.call(this);
}

F7QbeListDialog.prototype.drawGridInDialog = function(listContainer,listGrid){
  F7ListDialog.prototype.drawGridInDialog.call(this,listContainer, listGrid);
  var el = this.getElement();
  var that=this;
  var okButton = $$(el).find(".dialog-ok-button");
  if (okButton){
    $$(okButton).once("click",function(ev){
      that.defaultAction();
    });
  }
}

F7QbeListDialog.prototype.getSelectableGrid = function(listContainer, dialogCols, selectableF){
  return F7ListDialog.prototype.getSelectableGrid.call(this, listContainer, dialogCols, selectableF);
}

F7QbeListDialog.prototype.closeListDialog = function(){
  myApp.closeModal(this.currentDialog);
}